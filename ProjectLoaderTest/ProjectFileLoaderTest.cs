﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Someditc;
using Someditc.ProjectFiles;
using System.IO;
using System.Collections.Generic;


namespace ProjectLoaderTest
{
    [TestClass]
    public class ProjectFileLoaderTest
    {
        [TestMethod]
        public void TestProjectFileLoader()
        {
            // NOTE
            // This is the TEST's directory, not the normal project's.
            // Fine lessons learned from the smallest of obstacles.
            // Small things can still fuck you up royally.
            string directory = Directory.GetCurrentDirectory() + "\\";
            ProjectFileLoader projectLoader;
            bool result;

            //Thread.Sleep(10000);

            // TEST: Make sure it knows that the project file exists.
            result = File.Exists(directory + "testproj.som");
            Assert.IsTrue(result);

            // TEST: Project file existence check
            projectLoader = new ProjectFileLoader(directory + "testproj.som");
            Assert.IsNotNull(projectLoader);

            // TEST: Loader is okay check
            result = projectLoader.IsOkay();
            Assert.IsTrue(result);

            // So that it doesn't think the project's still open, eugh.
            projectLoader.Close();
        }

        [TestMethod]
        public void TestProjectFileLoader_GetFileCount()
        {
            string directory = Directory.GetCurrentDirectory() + "\\";
            ProjectFileLoader projectLoader;
            int value;

            projectLoader = new ProjectFileLoader(directory + "testproj.som");
            value = projectLoader.GetFileCount();
            Console.WriteLine(value);

            // TEST: File count is nonzero check
            Assert.AreNotEqual(0, value);

            projectLoader.Close();
        }

        [TestMethod]
        public void TestProjectFileLoader_GetFileSizes()
        {
            string directory = Directory.GetCurrentDirectory() + "\\";
            ProjectFileLoader projectLoader;
            List<int> sizes;
            int value;

            projectLoader = new ProjectFileLoader(directory + "testproj.som");
            sizes = projectLoader.GetFileSizes();

            foreach(int size in sizes)
            {
                Console.WriteLine(size);
            }

            value = sizes.Count;

            // TEST: At least one contained file with valid size check
            Assert.AreNotEqual(0, value);

            projectLoader.Close();
        }

        [TestMethod]
        public void TestProjectFileLoader_GetFileNames()
        {
            string directory = Directory.GetCurrentDirectory() + "\\";
            ProjectFileLoader projectLoader;
            List<string> names;
            int value;

            projectLoader = new ProjectFileLoader(directory + "testproj.som");
            names = projectLoader.GetFileNames();

            foreach(string name in names)
            {
                Console.WriteLine(name);
            }

            value = names.Count;

            // TEST: At least one named contained file check
            Assert.AreNotEqual(0, value);

            projectLoader.Close();
        }
    }
}
