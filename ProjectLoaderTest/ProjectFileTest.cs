﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Someditc;
using Someditc.ProjectFiles;
using System.IO;
using System.Threading;


namespace ProjectLoaderTest
{
    [TestClass]
    public class ProjectFileTest
    {
        [TestMethod]
        public void TestProjectFile_Load()
        {
            // NOTE
            // This is the TEST's directory, not the normal project's.
            // Fine lessons learned from the smallest of obstacles.
            // Small things can still fuck you up royally.
            string directory = Directory.GetCurrentDirectory() + "\\";
            ProjectFile projectFile;
            bool result;

            //Thread.Sleep(10000);

            // Make sure it knows it exists.
            result = File.Exists("ProjectLoader.exe");
            Assert.IsTrue(result);

            // Make sure it knows the number of files eeeh I dunno.
            //result = ((Directory.GetFiles(".")).Length == 4);
            //Assert.IsTrue(result);
            Console.WriteLine(directory);
            //printFiles(directory);


            // Make sure it knows that bullshit doesn't exist.
            result = File.Exists(directory + "bullshit.txt");
            Assert.IsFalse(result);

            // Make sure that attempting to load bullshit fails.
            projectFile = new ProjectFile(directory + "bullshit.txt");
            result = projectFile.Load();
            Assert.IsFalse(result);

            // Make sure it knows that the project file exists.
            result = File.Exists(directory + "testproj.som");
            Assert.IsTrue(result);

            projectFile = new ProjectFile(directory + "testproj.som");
            result = projectFile.Load();
            Assert.IsTrue(result);
        }

        private void printFiles(string directory)
        {
            string[] files = Directory.GetFiles(directory);
            for (int i = 0; i < files.Length; i++)
                Console.WriteLine(files[i]);
        }
    }
}
