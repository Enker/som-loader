﻿// 11-28-2019

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Someditc;
using Someditc.ProjectFiles;
using System.IO;


namespace ProjectLoaderTest
{
    [TestClass]
    public class RomContainedFileTest
    {
        [TestMethod]
        public void RomContainedFile_Header()
        {
            string directory = Directory.GetCurrentDirectory() + "\\";
            ProjectFile projectFile;
            RomContainedFile rcf;
            //bool result;

            projectFile = new ProjectFile(directory + "testproj.som");
            projectFile.Load();

            // Check rom retrieval from project, that it's not null.
            rcf = projectFile.GetRom();
            Assert.IsNotNull(rcf);

            // Check that header is present or not present.
            // Eugh, would like to separate this. Maybe use different
            // projects for testing, I dunno.
            if(rcf.HasExtraData())
                Assert.IsTrue(rcf.IsHeaderPresent());
            else
                Assert.IsFalse(rcf.IsHeaderPresent());

            // Check that positioning is correct.
            // Yes, indeed, that extra space padding is required.
            Assert.AreEqual(rcf.FetchGameTitle(), "Secret of MANA       ");
        }
    }
}
