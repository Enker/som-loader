﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Someditc.ProjectFiles
{
    public class RomContainedFile : ContainedFile
    {
        // Whether or not the 0x200 SMC header is present.
        public bool headerPresent;

        // Raw positioning in rom file, header needs
        // proper accounting for when using it.
        private int pos;

        //public bool loadSuccessful = false;
        //private RegionEnum region = RegionEnum.UNKNOWN;

        // The usual checksum at $FFDE (HiROM)
        private ushort givenChecksum = 0;

        // The newly calculated checksum.
        private ushort calculatedChecksum = 0;

        public RomContainedFile(string romname, byte[] romData)
        {
            this.data = romData;
            this.filename = romname;
            headerPresent = false;

            CheckForHeader();

            // ~~~ use this
            //loadSuccessful = verifyCorrectRom();
            ReadRomRegionAndStuff();

            //if (romData.Length < 4194304)
            //{
                //expand();
            //}

        }

        public bool ReadRomRegionAndStuff()
        {
            // ~~~ check language and stuff
            //bool ret = RegionSettings.romLoaded(this);
            bool ret = true;

            FetchGivenChecksum();

            CalculateChecksum();

            return ret;
            //generatedChecksum = checksum;
        }

        #region Rom Property Fetches
        public void CheckForHeader()
        {
            // need to expand
            if(HasExtraData())
            {
                headerPresent = true;
                pos = 0x200;
            }
        }

        public bool HasExtraData()
        {
            return (data.Length % 1024) != 0;
        }

        // Fetch the checksum from the rom.
        public void FetchGivenChecksum()
        {
            // Given checksum address (HiROM)
            Seek(0xFFDE);

            givenChecksum = ReadUInt16();

            Seek(0);
        }

        // Not so useful in its own right, but, useful for making
        // sure the raw position works alongside the header adjustment.
        public string FetchGameTitle()
        {
            string title;

            Seek(0xFFC0);

            // S'the SNES header, so using ASCII:
            title = Encoding.ASCII.GetString(ReadBytes(0x15));

            Seek(0);

            return title;
        }

        // Calculate a checksum independent of the one in the rom.
        public void CalculateChecksum()
        {
            calculatedChecksum = 0;
            int checksumCalcStart = 0;

            if(headerPresent)
            {
                checksumCalcStart += 0x200;
            }

            while(checksumCalcStart < data.Length)
            {
                calculatedChecksum += data[checksumCalcStart];
                checksumCalcStart++;
            }

        }
        #endregion

        #region Getters
        // Check if SMC header is present in the rom file.
        public bool IsHeaderPresent()
        {
            return headerPresent;
        }

        // Get the rom-supplied checksum.
        public ushort GetGivenChecksum()
        {
            return givenChecksum;
        }

        // Get the independently calculated checksum.
        public ushort GetCalculatedChecksum()
        {
            return calculatedChecksum;
        }
        #endregion

        #region Seek Methods
        // pos in header-present format.
        public void SeekInHeaderRom(int pos)
        {
            this.pos = headerPresent ? (pos + 0x200) : pos;
        }
        public void Seek(int pos)
        {
            SeekInHeaderRom(pos);
        }

        public int readPos()
        {
            if(headerPresent)
                return pos + 0x200;
            else
                return pos;
        }
        #endregion

        #region Read Methods
        // Methods in this region all use raw positioning.
        // Make sure your header handling is working properly!

        public void Read(byte[] buffer, int index, int count)
        {
            for (int i = 0; i < count; i++)
            {
                buffer[i + index] = data[pos];
                pos++;
            }
        }

        public ushort ReadUInt16()
        {
            return (ushort)(data[pos++] + data[pos++] * 256);
        }

        public byte ReadByte()
        {
            return data[pos++];
        }

        public uint ReadUInt32()
        {
            return (uint)(data[pos++] + data[pos++] * 256 + data[pos++] * 256 * 256 + data[pos++] * 256 * 256 * 256);
        }

        public char ReadChar()
        {
            return (char)ReadByte();
        }

        public short ReadInt16()
        {
            return (short)ReadUInt16();
        }

        public byte[] ReadBytes(int number)
        {
            byte[] ret = new byte[number];
            for (int i = 0; i < number; i++)
            {
                ret[i] = data[pos++];
            }
            return ret;
        }
        #endregion

        public override string ToString()
        {
            return "[RCF:" + filename;
        }

        #region Old Shit
        /*public RegionEnum Region
{
    get
    {
        return region;
    }
}*/
        // ~~~ OLD.
        private void expand()
        {
            /*
            // ~~~ needs to support header

            ////Add 2 mb to the file
            //FileStream fs = new FileStream(romname, System.IO.FileMode.Append);
            //RomWrite = new BinaryWriter(fs);
            //byte[] empty = new byte[1024 * 1024 * 2];
            //RomWrite.Write(empty);
            int headerOffset = headerPresent ? 512 : 0;
            int headerBackOffset = headerPresent ? 0 : 512;
            byte[] expandedData = new byte[1024 * 1024 * 4 + headerOffset];
            for (int i = 0; i < 1024 * 1024 * 2 + headerOffset; i++)
            {
                expandedData[i] = data[i];
            }


            ////Re-open it for r/w
            //fs.Close();
            //fs = new FileStream(romname, System.IO.FileMode.Open);
            ////RomRead = new BinaryReader(fs);
            ////RomWrite = new BinaryWriter(fs);

            ////EVENTS
            ////RomRead.BaseStream.Seek(DataGlobals.eventsOriginalStart, SeekOrigin.Begin);
            int start = GlobalOffsetsAndStuff.eventsOriginalStart;
            int end = GlobalOffsetsAndStuff.eventsOriginalSize;
            int offset = GlobalOffsetsAndStuff.eventsNewStart - GlobalOffsetsAndStuff.eventsOriginalStart;
            for (int i = start - headerBackOffset; i < start + end - headerBackOffset; i++)
            {
                expandedData[i + offset] = data[i];
            }

            for (int i = 0; i < GlobalOffsetsAndStuff.eventsMsbPointers.Length; i++)
            {
                expandedData[GlobalOffsetsAndStuff.eventsMsbPointers[i] - headerBackOffset] = 0xE2;
            }
            ////re-arrange some shit.
            //byte[] events = RomRead.ReadBytes(DataGlobals.eventsOriginalSize);
            //RomWrite.BaseStream.Seek(DataGlobals.eventsNewStart, SeekOrigin.Begin);
            //RomWrite.Write(events);
            ////change a couple pointers in the code
            //// ~~~ could be improved
            //for (int i = 0; i < DataGlobals.eventsMsbPointers.Length; i++)
            //{
            //    RomWrite.BaseStream.Seek(DataGlobals.eventsMsbPointers[i], SeekOrigin.Begin);
            //    RomWrite.Write((byte)0xE2);
            //}
            ////Zero out the old data?

            start = GlobalOffsetsAndStuff.mapsOriginalStart;
            end = GlobalOffsetsAndStuff.mapsOriginalSize;
            offset = GlobalOffsetsAndStuff.mapsNewStart - GlobalOffsetsAndStuff.mapsOriginalStart;
            for (int i = start - headerBackOffset; i < start + end - headerBackOffset; i++)
            {
                expandedData[i + offset] = data[i];
            }
            for (int i = 0; i < GlobalOffsetsAndStuff.mapsMsbPointers.Length; i++)
            {
                expandedData[GlobalOffsetsAndStuff.mapsMsbPointers[i]] = 0xF0;
            }
            ////MAPS
            //RomRead.BaseStream.Seek(DataGlobals.mapsOriginalStart, SeekOrigin.Begin);
            //byte[] maps = RomRead.ReadBytes(DataGlobals.mapsOriginalSize);
            //RomWrite.BaseStream.Seek(DataGlobals.mapsNewStart, SeekOrigin.Begin);
            //RomWrite.Write(maps);
            ////change assembly pointers (3)
            //for (int i = 0; i < DataGlobals.mapsMsbPointers.Length; i++)
            //{
            //    RomWrite.BaseStream.Seek(DataGlobals.mapsMsbPointers[i], SeekOrigin.Begin);
            //    RomWrite.Write((byte)0xF0);
            //}

            for (int i = 0; i < 59; i++)
            {
                data[GlobalOffsetsAndStuff.musicOffsetsStart - headerBackOffset + i * 3] += 0x37;
            }
            start = GlobalOffsetsAndStuff.musicOffsetsStart;
            end = GlobalOffsetsAndStuff.musicOriginalSize;
            offset = GlobalOffsetsAndStuff.musicNewStart - GlobalOffsetsAndStuff.musicOffsetsStart;
            for (int i = start - headerBackOffset; i < start + end - headerBackOffset; i++)
            {
                expandedData[i + offset] = data[i];
            }
            ////MUSIC
            //for (int i = 0; i < 59; i++)
            //{
            //    RomRead.BaseStream.Seek(DataGlobals.musicOffsetsStart + i * 3, SeekOrigin.Begin);
            //    byte v = RomRead.ReadByte();
            //    // ~~~ could be improved
            //    v += 0x37;
            //    RomWrite.BaseStream.Seek(DataGlobals.musicOffsetsStart + i * 3, SeekOrigin.Begin);
            //    RomWrite.Write(v);
            //}

            //RomRead.BaseStream.Seek(DataGlobals.musicOriginalStart, SeekOrigin.Begin);
            //byte[] music = RomRead.ReadBytes(DataGlobals.musicOriginalSize);
            //RomWrite.BaseStream.Seek(DataGlobals.musicNewStart, SeekOrigin.Begin);
            //RomWrite.Write(music);

            ////patch thing, replaces events ~~~ what is this?
            //RomWrite.BaseStream.Seek(DataGlobals.eventsOriginalStart, SeekOrigin.Begin);
            //RomWrite.Write((uint)0);
            for (int i = GlobalOffsetsAndStuff.eventsOriginalStart - headerBackOffset; i < GlobalOffsetsAndStuff.eventsOriginalStart - headerBackOffset + 4; i++)
            {
                expandedData[i] = 0;
            }

            data = expandedData;
            //fs.Close();
            //StatusMessage("ROM expanded successfully.");
             */
        }
        #endregion
    }
}
