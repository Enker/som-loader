﻿/// <summary>
/// Abstract class for the series of ContainedFiles, the internally
/// stored files for each and every little thing in the SoMEdit
/// project file.
/// Has filename, id for multiples of a type, and a byte array for file data.
/// XML Comments first placed: 1/29/2018
/// </summary>

namespace Someditc.ProjectFiles
{
    public abstract class ContainedFile
    {
        public string filename;
        public int id; // index in whatever list of things (song 4 for example)
        public byte[] data;

    }
}
