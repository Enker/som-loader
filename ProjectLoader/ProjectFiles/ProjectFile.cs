﻿/// <summary>
/// ProjectFile class, for holding contents of the project file.
/// Said contents are largely internally contained files.
/// Crucial to understanding how SoMEdit sorts all of its data.
/// XML Comments first placed: 1/29/2018
/// </summary>

using System.IO;
using System;
using System.Collections.Generic;

namespace Someditc.ProjectFiles
{
    public class ProjectFile
    {
        ContainedFileManager cfm;
        // List of contained files in project file.
        List<ContainedFile> containedFiles = new List<ContainedFile>();

        // Project file itself.
        private readonly string sourceFileName;
        private int numFiles;

        /// <summary>
        /// #CONSTRUCTOR
        /// Standard constructor for Project File.</summary>
        /// <param name="sourceFileName">Project file to open for this.</param>
        public ProjectFile(string sourceFileName)
        {
            Console.WriteLine(">>Creating ProjectFile ob for " + sourceFileName + ".");
            this.sourceFileName = sourceFileName;
            //load();
        }

        public bool Load()
        {
            ProjectFileLoader loader;
            bool ok = true;

            Console.WriteLine(">>ProjectFile.Load()");

            if(File.Exists(sourceFileName))
            {
                // Make a BinaryReader with our project file.
                //FileStream fs = new FileStream(sourceFileName, FileMode.Open);
                //BinaryReader br = new BinaryReader(fs);
                loader = new ProjectFileLoader(sourceFileName);
                if(loader.IsOkay())
                {
                    ok = true;
                    numFiles = loader.GetFileCount();
                    containedFiles = loader.GetContainedFiles();
                    cfm = new ContainedFileManager(containedFiles);
                    Console.WriteLine(">>>Loader okay! Num files: " + numFiles + ", Files: " + loader.WriteFileList() + ".");
                }

                if(loader != null)
                {
                    Console.WriteLine(">>Idly, rom: " + GetRom().ToString() + ".");
                    Console.WriteLine(">>Nothing else to do, closing project.");
                    loader.Close();
                    Console.WriteLine("Closed project.");
                }
            }
            else
            {
                Console.WriteLine(">>ProjectFile.Load() -- Something went wrong.");
                ok = false;
            }

            return ok;
        }

        /// <summary>
        /// Get the contained file for the SoM rom from the List.</summary>
        /// <returns>The contained file for the rom, null if not found.</returns>
        public RomContainedFile GetRom()
        {
            return (RomContainedFile)cfm.GetFileByPrefix(".smc");
        }

        /*
        /// <summary>
        /// Write all contained file data to the project file.</summary>
        /// <seealso cref="ProjectFile.write(string)"/>
        public void write()
        {
            write(sourceFileName);
        }

        /// <summary>
        /// Write all contained file data to an outfile.
        /// </summary>
        /// <param name="outFileName">Name of the outfile to write to.</param>
        public void write(string outFileName)
        {
            FileStream fs = new FileStream(outFileName, FileMode.Create);
            BinaryWriter bw = new BinaryWriter(fs);

            // COUNT SECTION
            // 1. Write the count of contained files to the outfile.
            bw.Write(containedFiles.Count);

            // LENGTH SECTION
            // 2. Write length of filename, filename, and length of data.
            foreach (ContainedFile cf in containedFiles)
            {
                bw.Write(cf.filename.Length);
                bw.Write(cf.filename.ToCharArray());
                bw.Write(cf.data.Length);
            }

            // DATA SECTION
            // 3. Write the data of each contained file to the outfile.
            foreach (ContainedFile cf in containedFiles)
            {
                bw.Write(cf.data);
            }
            bw.Flush();
            bw.Close();
            fs.Close();
        }
        #endregion

        #endregion
        */
    }
}
