﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Someditc.ProjectFiles
{
    public class ProjectFileLoader
    {
        List<ContainedFile> containedFiles = new List<ContainedFile>();

        private string sourceFileName;
        FileStream fs;
        BinaryReader br;

        int fileCount;
        List<int> fileSizes;
        List<string> fileNames;

        public ProjectFileLoader(string sourceFileName)
        {
            try
            {
                this.sourceFileName = sourceFileName;
                // Make a BinaryReader with our project file.
                fs = new FileStream(sourceFileName, FileMode.Open);
                br = new BinaryReader(fs);

                // Contained file count
                fileCount = ReadFileCount();

                // Contained file sizes
                // Contained file names
                ReadFileSizesAndNames();

                // Split data up into particular contained files
                ReadAndCreateContainedFiles();
            }
            catch(IOException ioe)
            {
                Console.WriteLine(ioe);
            }
        }

        public void Close()
        {
            if(br != null) br.Close();
            if(br != null) fs.Close();
            sourceFileName = null;
        }

        // Check if the loader's okay over all, i.e.,
        // 1. the source file exists
        // 2. a FileStream was opened successfully for it,
        // 3. a BinaryWriter was opened successfully for it.
        // ...that might be overkill, but this shit is critical.
        public bool IsOkay()
        {
            if(!File.Exists(sourceFileName))
            {
                Console.WriteLine("Couldn't find file: " + sourceFileName);
                return false;
            }
            if(fs == null)
            {
                Console.WriteLine("Couldn't open FileStream.");
                return false;
            }
            if(br == null)
            {
                Console.WriteLine("Couldn't open BinaryReader.");
                return false;
            }

            return true;
        }

        private int ReadFileCount()
        {
            Console.WriteLine(">>>Reading file count.");

            if(br == null) return -1;
            if(!fs.CanSeek) return -1;

            fs.Seek(0, SeekOrigin.Current);

            // Contained file count
            int numFiles = br.ReadInt32();
            //LoggingServer.logDebug("Num files = " + numFiles, "project");
            return numFiles;
        }

        // Read all contained file names and their sizes.
        private void ReadFileSizesAndNames()
        {
            int numFiles = fileCount;

            // Contained file sizes
            List<int> sizes = new List<int>();

            // Contained file names
            List<string> names = new List<string>();

            Console.WriteLine(">>>Reading file names and sizes.");

            // Loop which gets all contained filenames and lengths.
            for(int i = 0; i < numFiles; i++)
            {
                int filenameLength = br.ReadInt32();
                //LoggingServer.logDebug("filename length = " + filenameLength, "project");

                string filename = new string(br.ReadChars(filenameLength));
                //LoggingServer.logDebug("filename = " + filename, "project");

                sizes.Add(br.ReadInt32());
                names.Add(filename);
            }

            fileSizes = sizes;
            fileNames = names;

            Console.WriteLine(">>>SUCCESS: sizes " + sizes.ToString() + ", names: " + names.ToString() + ".");
        }

        public bool ReadAndCreateContainedFiles()
        {
            bool ok = false;
            int numFiles = fileCount;

            // Loop for reading contained files from the project
            // file into relevant classes.
            for(int i = 0; i < numFiles; i++)
            {
                int size = fileSizes[i];
                string name = fileNames[i];
                byte[] fileData = br.ReadBytes(size);

                // GREAT CHAIN OF IF-ELSES
                // Used for handling of specific contained file types.
                // ~~~ change to static handlers

                // Type files: multiples of each.
                if(name.Equals("rom.smc"))
                {
                    RomContainedFile rcf = new RomContainedFile("rom.smc", fileData);
                    ok = rcf.ReadRomRegionAndStuff();
                    containedFiles.Add(rcf);
                }
            }

            return ok;
        }

        /*
  #region File I/O
  /// <summary>
  /// #LOAD
  /// Load all contained files in the project file into
  /// classes for SoMEdit to fiddle with.
  /// </summary>
  /// <returns></returns>
  public bool load()
  {
      bool ok = true;

      if (File.Exists(sourceFileName))
      {
          // Make a BinaryReader with our project file.
          FileStream fs = new FileStream(sourceFileName, FileMode.Open);
          BinaryReader br = new BinaryReader(fs);

          // Contained file count
          int numFiles = br.ReadInt32();
          LoggingServer.logDebug("Num files = " + numFiles, "project");

          // Contained file sizes
          List<int> sizes = new List<int>();

          // Contained file names
          List<string> names = new List<string>();

          // Loop which gets all contained filenames and lengths.
          for (int i = 0; i < numFiles; i++)
          {
              int filenameLength = br.ReadInt32();
              LoggingServer.logDebug("filename length = " + filenameLength, "project");

              string filename = new string(br.ReadChars(filenameLength));
              LoggingServer.logDebug("filename = " + filename, "project");

              sizes.Add(br.ReadInt32());
              names.Add(filename);
          }

          // Loop for reading contained files from the project
          // file into relevant classes.
          for (int i = 0; i < numFiles; i++)
          {
              int size = sizes[i];
              string name = names[i];
              byte[] fileData = br.ReadBytes(size);

              // GREAT CHAIN OF IF-ELSES
              // Used for handling of specific contained file types.
              // ~~~ change to static handlers

              // Type files: multiples of each.
              if (name.StartsWith("song_"))
              {
                  string[] sp = name.Split(new char[]{'_'});
                  int songNum = int.Parse(sp[1]);
                  SongContainedFile scf = new SongContainedFile(name, fileData, songNum);
                  containedFiles.Add(scf);
              }
              else if (name.StartsWith("event_"))
              {
                  string[] sp = name.Split(new char[] { '_' });
                  int eventNum = int.Parse(sp[1]);
                  EventContainedFile ecf = new EventContainedFile(name, fileData, eventNum);
                  containedFiles.Add(ecf);
              }
              else if (name.StartsWith("tileset8_"))
              {
                  string[] sp = name.Split(new char[] { '_' });
                  int tilesetNum = int.Parse(sp[1]);
                  Tileset8ContainedFile tcf = new Tileset8ContainedFile(name, fileData, tilesetNum);
                  containedFiles.Add(tcf);
              }
              else if (name.StartsWith("tileset16_"))
              {
                  string[] sp = name.Split(new char[] { '_' });
                  int tilesetNum = int.Parse(sp[1]);
                  Tileset16ContainedFile tcf = new Tileset16ContainedFile(name, fileData, tilesetNum);
                  containedFiles.Add(tcf);
              }
              else if (name.StartsWith("sample_"))
              {
                  string[] sp = name.Split(new char[] { '_' });
                  int sampleNum = int.Parse(sp[1]);
                  SampleContainedFile scf = new SampleContainedFile(name, fileData, sampleNum);
                  containedFiles.Add(scf);
              }
              else if (name.StartsWith("mappiece_"))
              {
                  string[] sp = name.Split(new char[] { '_' });
                  int pieceNum = int.Parse(sp[1]);
                  MapPieceContainedFile mpcf = new MapPieceContainedFile(name, fileData, pieceNum);
                  containedFiles.Add(mpcf);
              }
              else if (name.StartsWith("fullmap_"))
              {
                  string[] sp = name.Split(new char[] { '_' });
                  int pieceNum = int.Parse(sp[1]);
                  FullMapContainedFile mpcf = new FullMapContainedFile(name, fileData, pieceNum);
                  containedFiles.Add(mpcf);
              }

              // Singular files: global entities, self-contained arrays.
              else if (name.Equals("worldmap"))
              {
                  WorldMapContainedFile wmcf = new WorldMapContainedFile(name, fileData);
                  containedFiles.Add(wmcf);
              }
              else if (name.Equals("font"))
              {
                  FontContainedFile fcf = new FontContainedFile(name, fileData);
                  containedFiles.Add(fcf);
              }
              else if (name.Equals("tile"))
              {
                  TileContainedFile fcf = new TileContainedFile(name, fileData);
                  containedFiles.Add(fcf);
              }
              else if (name.Equals("tilesetpalettes"))
              {
                  TilesetPalettesContainedFile fcf = new TilesetPalettesContainedFile(name, fileData);
                  containedFiles.Add(fcf);
              }
              else if (name.Equals("doors"))
              {
                  DoorContainedFile fcf = new DoorContainedFile(name, fileData);
                  containedFiles.Add(fcf);
              }
              else if (name.Equals("displaysettings"))
              {
                  DisplaySettingsContainedFile fcf = new DisplaySettingsContainedFile(name, fileData);
                  containedFiles.Add(fcf);
              }
              //else if (name.StartsWith("projectsettings_"))
              //{
                  //string[] sp = name.Split(new char[] { '_' });
                  //int pieceNum = int.Parse(sp[1]);
                  //ProjectSettingsContainedFile mpcf = new ProjectSettingsContainedFile(name, fileData, pieceNum);
                  //containedFiles.Add(mpcf);
              //}
              else if (name.Equals("rom.smc"))
              {
                  RomContainedFile rcf = new RomContainedFile("rom.smc", fileData);
                  ok = rcf.readRomRegionAndStuff();
                  containedFiles.Add(rcf);
              }
              else
              {
                  // unknown file - assume settings file
                  ProjectSettingsContainedFile mpcf = new ProjectSettingsContainedFile(name, fileData);
                  containedFiles.Add(mpcf);
              }
              //cf.filename = name;
              //cf.data = fileData;
          }
      }
      return ok;
  */

        #region Getters
        public int GetFileCount() { return fileCount; }

        public List<int> GetFileSizes() { return fileSizes; }

        public List<string> GetFileNames() { return fileNames; }

        public List<ContainedFile> GetContainedFiles()
        {
            return containedFiles;
        }
        #endregion

        public String WriteFileList()
        {
            String listString = "";

            // Make it known if there aren't any contained files.
            if(containedFiles.Count == 0)
            {
                return "(none)";
            }

            foreach(ContainedFile cf in containedFiles)
            {
                listString += cf.filename;
            }

            return listString;
        }
    }
}
