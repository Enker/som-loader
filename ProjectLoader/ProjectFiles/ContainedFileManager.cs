using System;
using System.Collections.Generic;
using System.IO;

namespace Someditc.ProjectFiles
{
    public class ContainedFileManager
    {
        // List of contained files.
        List<ContainedFile> containedFiles = new List<ContainedFile>();

        public ContainedFileManager(List<ContainedFile> cfs)
        {
            containedFiles = cfs;
        }

        public bool HasContainedFile(Type t, string name)
        {
            foreach (ContainedFile cf in containedFiles)
            {
                if (cf.GetType().Equals(t) && cf.filename == name)
                {
                    return true;
                }
            }
            return false;
        }

        /*
        /// <summary>
        /// Looks through the List of contained files for the project settings file,
        /// verifies that a particular setting is present in it.
        /// #REFACTOR: this looks like it could be done better, but how, hmmm.
        /// </summary>
        /// <param name="settingsName">True if the setting's present, false if otherwise.</param>
        /// <returns></returns>
        public bool HasProjectSettings(string settingsName)
        {
            foreach(ContainedFile cf in containedFiles)
            {
                if(cf.GetType().Equals(typeof(ProjectSettingsContainedFile)) && cf.filename == settingsName)
                {
                    return true;
                }
            }
            return false;
        }

                    /// <summary>
        /// Gets a project settings file for the specified setting.
        /// </summary>
        /// <param name="settingsName">The kind of setting file we want to get.</param>
        /// <returns>A ProjectsSettingsContainedFile for the specified setting.
        /// ...or, if it can't find the setting, it adds it to the List of contained files
        /// and returns a blank settings object. Huh.</returns>
        public ProjectSettingsContainedFile getProjectSettings(string settingsName)
        {
            foreach (ContainedFile cf in containedFiles)
            {
                if (cf.GetType().Equals(typeof(ProjectSettingsContainedFile)) && cf.filename == settingsName)
                {
                    return (ProjectSettingsContainedFile)cf;
                }
            }
            // blank one
            ProjectSettingsContainedFile pscf = new ProjectSettingsContainedFile(settingsName);
            containedFiles.Add(pscf);
            return pscf;
        }
        */

        public ContainedFile GetFileByPrefix(string prefix)
        {
            foreach (ContainedFile cf in containedFiles)
            {
                if (cf.filename.EndsWith(prefix))
                {
                    return cf;
                }
            }

            return null;
        }

        #region Getters

        public T GetFileByName<T>(Type t, string name)
        {
            foreach (ContainedFile cf in containedFiles)
            {
                if (cf.GetType().Equals(t) && cf.filename == name)
                {
                    // ~~~ how to declare T to be a subclass of ContainedFile?  this works for now
                    return (T)((object)cf);
                }
            }
            // ffs
            return (T)((object)null);
        }

        /// <summary>
        /// Gets a file based upon a ContainedFile class and an ID number, huh. Neat.
        /// #REFACTOR: could maybe be a little more efficient, but, eh.
        /// </summary>
        /// <typeparam name="T">The ContainedFile type of the file.</typeparam>
        /// <param name="t">The ContainedFile type of the file. Huh.</param>
        /// <param name="id">Numerical ID associated with the file.</param>
        /// <returns>A contained file of the particular type specified by T.</returns>
        /// <seealso cref="ContainedFileManager.getFilesOfType{T}(Type)"/>
        public T GetFileById<T>(Type t, int id)
        {
            foreach (ContainedFile cf in containedFiles)
            {
                if (cf.GetType().Equals(t) && cf.id == id)
                {
                    // ~~~ how to declare T to be a subclass of ContainedFile?  this works for now
                    return (T)((object)cf);
                }
            }
            // ffs
            return (T)((object)null);
        }

        /// <summary>
        /// Gets a List of all files of a ContainedFile type.
        /// </summary>
        /// <typeparam name="T">The ContainedFile type of the files.</typeparam>
        /// <param name="t">The ContainedFile type of the files. ...huh.</param>
        /// <returns>A list of contained files of the specified type.</returns>
        /// <seealso cref="ContainedFileManager.getFileById{T}(Type, int)"/>
        public List<T> GetFilesOfType<T>(Type t)
        {
            List<T> retList = new List<T>();
            foreach (ContainedFile cf in containedFiles)
            {
                if (cf.GetType().Equals(t))
                {
                    // ~~~ how to declare T to be a subclass of ContainedFile?  this works for now
                    retList.Add((T)((object)cf));
                }
            }
            return retList;
        }

        /// <summary>
        /// Get a contained file's data from the List of contained files.
        /// </summary>
        /// <param name="filename">Name of the contained file to get data for.</param>
        /// <returns>Byte array if found, null if not.</returns>
        public byte[] GetFile(string filename)
        {
            foreach (ContainedFile cf in containedFiles)
            {
                if (cf.filename.Equals(filename))
                {
                    return cf.data;
                }
            }
            return null;
        }
        #endregion

        #region Adding/Removing
        /// <summary>
        /// Add a file to the List of contained files.
        /// Removes the file from the List first just in case.
        /// </summary>
        /// <param name="newfile">ContainedFile to be added.</param>
        public void AddFile(ContainedFile newFile)
        {
            // Use the ContainedFile's filename here rather than
            // passing it in. Less room to trip up that way.
            RemoveFile(newFile.filename);

            containedFiles.Add(newFile);
        }

        #region Old addFile
        //public void addFile(string filename, byte[] data)
        //{
        //    removeFile(filename);
        //    ContainedFile newFile;
        //    //if (filename.EndsWith(".smc"))
        //    //{
        //        //newFile = new RomContainedFile();
        //        //if (data.Length > 2097152)
        //        //{
        //            //((RomContainedFile)newFile).headerPresent = true;
        //        //}
        //    //}
        //    //else
        //    //{
        //        newFile = new ContainedFile();
        //    //}
        //    newFile.filename = filename;
        //    newFile.data = data;
        //    containedFiles.Add(newFile);
        //}
        #endregion

        /// <summary>
        /// Removes a file from the List of contained files.
        /// </summary>
        /// <param name="filename">Name of the file to be removed.</param>
        public void RemoveFile(string filename)
        {
            ContainedFile found = null;

            // Look for the contained file with the passed in filename.
            foreach (ContainedFile cf in containedFiles)
            {
                if (cf.filename.Equals(filename))
                {
                    found = cf;
                    break;
                }
            }

            // Remove the contained file if found.
            if (found != null)
            {
                containedFiles.Remove(found);
            }
        }
        #endregion

        public int GetFileCount()
        {
            return containedFiles.Count;
        }

        public void WriteHeaders(BinaryWriter bw)
        {
            if(bw == null) return;

            foreach (ContainedFile cf in containedFiles)
            {
                bw.Write(cf.filename.Length);
                bw.Write(cf.filename.ToCharArray());
                bw.Write(cf.data.Length);
            }
        }

        public void WriteDataSections(BinaryWriter bw)
        {
            if(bw == null) return;

            foreach (ContainedFile cf in containedFiles)
            {
                bw.Write(cf.data);
            }
        }
    }
}