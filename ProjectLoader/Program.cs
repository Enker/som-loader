﻿using System;
using Someditc.ProjectFiles;

namespace Someditc
{
    public class Program
    {
        public static ProjectFile currentProject;
        //public static RomContainedFile currentRom;
        //string romname;

        public Program(string[] commandLineArgs)
        {
            //LoggingServer.logDebug("Processing command-line arguments for input file", "general");
            if(commandLineArgs != null)
            {
                for(int i = 0; i < commandLineArgs.Length; i++)
                {
                    if(commandLineArgs[i].EndsWith(".som"))
                    {
                        //LoggingServer.logDebug("Loading command-line file", "general");
                        LoadProjectFile(commandLineArgs[i]);
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            new Program(args);
        }

        private void LoadProjectFile(string filename)
        {
            Console.WriteLine(">Attempting to load " + filename + ".");

            try
            {
                //LoggingServer.log(TRANSLATION.getMessage("statusMessage.projectLoading", filename));
                ProjectFile file = new ProjectFile(filename);
                if (!file.Load())
                {
                    return;
                }
                currentProject = file;
                /*currentRom = file.getRom();*/
                //file.addFile(romFile);
                //file.write();
                //currentProject = file;
                //currentRom = romFile;
                /*romname = currentRom.filename;*/
                //panel2.Controls.Clear();
                //categoriesTree.SelectedNode = null;

                /*
                List<Form> windows = new List<Form>();
                foreach (string key in otherWindows.Keys)
                {
                    windows.Add(otherWindows[key]);
                }
                foreach (Form f in windows)
                {
                    f.Close();
                }
                */

                //menuStrip1.Items.Clear();
                //menuStrip1.Items.Add(fileToolStripMenuItem);
                //menuStrip1.Items.Add(helpToolStripMenuItem);
                //menuStrip1.Items.Add(languageMenuItemBase);
                bool success = OpenROM();
                if (success)
                {
                    //categoriesTree.Enabled = true;

                    //LoggingServer.log(TRANSLATION.getMessage("statusMessage.projectLoaded", filename));
                    //refreshWindowTitle();
                    //AddRecentFile(currentProject.sourceFileName);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                //StatusMessage(ex.ToString());
            }

        }

        // Valid method stub for the time being.
        public bool OpenROM() {
            //StatusMsg("Loading samples");
            //dummyEditor.LoadStuff(currentProject);
            return false;
        }
        /*
        //assumes it exists, and is an actual som rom
        //read all our romshit here.
        public bool OpenROM()
        {
            try
            {
                LoggingServer.logDebug("Initializing FMOD", "general");
                SNESSample.FSOUND_Init(44100, 64, 2);
            }
            catch (Exception e)
            {
                FSOUND_AVAILABLE = false;
                //StatusMsg("Note: FMOD.DLL not found or invalid.  Audio playback will not be available.");
                LoggingServer.log("Note: FMOD.DLL not found or invalid.  Audio playback will not be available.");
            }
            if (FSOUND_AVAILABLE)
                SNESSample.FSOUND_SetOutput(2); //directsound
            */
        #region FSOUND Notes
        //SNESSample.FSOUND_SetDriver(1);
        //SNESSample.FSOUND_SetMixer(0);
        //LoggingServer.enableDebugForThisMethod();
        //LoggingServer.log("HI");
        //MessageBox.Show("HI");

        /*
        SNESSample.ReverbStruct reverb;
        reverb.Environment = 6;
        reverb.EnvSize = 21.6f;
        reverb.EnvDiffusion = 1.00f;
        reverb.Room = -1000;
        reverb.RoomHF = -476;
        reverb.RoomLF = 0;
        reverb.DecayTime = 4.32f;
        reverb.DecayHFRatio = 0.59f;
        reverb.DecayLFRatio = 1.0f;
        reverb.Reflections = -789;
        reverb.ReflectionsDelay = 0.020f;
        reverb.ReflectionsPan = new float[] {0.0f, 0.0f, 0.0f};
        reverb.Reverb = -289;
        reverb.ReverbDelay = 0.030f;
        reverb.ReverbPan = new float[] {0.0f, 0.0f, 0.0f};
        reverb.EchoTime = 0.250f;
        reverb.EchoDepth = 0.00f;
        reverb.ModulationTime = 0.25f;
        reverb.ModulationDepth = 0.000f;
        reverb.AirAbsorptionHF = -5.0f;
        reverb.HFReference = 5000.0f;
        reverb.LFReference = 250.0f;
        reverb.RoomRolloffFactor = 0.0f;
        reverb.Diffusion = 100.0f;
        reverb.Density = 100.0f;
        reverb.Flags = 0x3FF;*/
        //SNESSample.FSOUND_Reverb_SetProperties(ref reverb);

        //{6, 21.6, 1.00, -1000, -476, 0, 4.32, 0.59, 1.0, -789, 0.020,
        //( 0.0,0.0,0.0 ), -289, 0.030, ( 0.0,0.0,0.0 ), 0.250, 0.00, 0.25, 0.000, -5.0, 5
        //000.0, 250.0, 0.0, 100.0, 100.0, 0x3ff}

        //int fx1 = SNESSample.FSOUND_FX_Enable(-3, 8);
        //int fx2 = SNESSample.FSOUND_FX_Enable(2, 8);
        //int fx3 = SNESSample.FSOUND_FX_Enable(3, 8);
        //int fx4 = SNESSample.FSOUND_FX_Enable(4, 8);
        //SNESSample.FSOUND_FX_SetEcho(fx1, 50.0f, 50.0f, 333.0f, 333.0f, 0);
        //SNESSample.FSOUND_FX_SetEcho(fx2, 50.0f, 50.0f, 333.0f, 333.0f, 0);
        //SNESSample.FSOUND_FX_SetEcho(fx3, 50.0f, 50.0f, 333.0f, 333.0f, 0);
        //SNESSample.FSOUND_FX_SetEcho(fx4, 50.0f, 50.0f, 333.0f, 333.0f, 0);

        #endregion

        // None of this shit should happen without the project file
        // getting loaded first.

        /*
        //currentRom.Seek(0x101D9);
        // http://patpend.net/technical/snes/sneskart.html
        // 00: JAP
        // 01: USA
        // 09: GER
        //byte languageIndicator = currentRom.ReadByte();

        else if (RegionSettings.LOADED_REGION == RegionSettings.RegionEnum.UNKNOWN)
        {
            LoggingServer.log("Unknown region detected.  Bad ROM?");
            return false;
        }
        //if (languageIndicator == 0x09)
        //{
        //LoggingServer.log("German ROM detected.  This ROM is not supported yet!  Try again later.");
        //return false;
        //}

        // DO NONE OF THIS, DO NOT EVEN ENABLE EDITORS UNTIL A PROJECT FILE
        // WITH A WORKING ROM AND CONTAINED FILES IS AVAILABLE.

        char[] s = { '\\' };
        string[] romtext = romname.Split(s);
        LoggingServer.logDebug("Loading stuff", "general");
        LoadMopsPatches();
        LoggingServer.logDebug("Loading names of things", "general");
        GlobalOffsetsAndStuff.LoadThingNames(); // ~~~ remove
        LoggingServer.logDebug("Loading SPC Samples", "general");
        //StatusMsg("Loading samples");
        sampleEditor.LoadSamples(currentProject);
        LoggingServer.logDebug("Loading Music", "general");
        //StatusMsg("Loading music");
        musicEditor.LoadMusic(currentProject);
        LoggingServer.logDebug("Loading Sound Effects", "general");
        soundEffectEditor.LoadEffects(currentProject);
        LoggingServer.logDebug("Loading Events", "general");
        //StatusMsg("Loading events");
        eventEditor.LoadEvents(currentProject);
        //eventEditor.ShowEvent(0x102);
        //eventEditor.ShowEvent(0x101);
        //StatusMsg("Loading chars");
        //MapEditorControl.LoadAll8x8Tiles();
        //StatusMsg("Loading TS");
        LoggingServer.logDebug("Loading Tilesets", "general");
        tileManager.LoadTilesets(currentProject);
        LoggingServer.logDebug("Loading Sprites", "general");
        spriteEditor.LoadCharacters(currentProject);
        LoggingServer.logDebug("Loading Collisions", "general");
        collisionLoader.LoadCollision(currentRom);
        LoggingServer.logDebug("Loading Display Settings", "general");
        displaySettingsEditor.loadDisplaySettings(currentProject);
        LoggingServer.logDebug("Loading Doors", "general");
        doorEditor.LoadDoors(currentProject);
        LoggingServer.logDebug("Loading Map Pieces", "general");
        //StatusMsg("Loading maps");
        mapPieceEditor.LoadMaps(currentProject);
        LoggingServer.logDebug("Loading Composite Maps", "general");
        //StatusMsg("Loading FMs");
        mapEditor.LoadFullMaps(currentProject);
        LoggingServer.logDebug("Loading World Map", "general");
        //StatusMsg("Loading WM");
        worldMapEditor.LoadWorldMap(currentProject);
        LoggingServer.logDebug("Loading Title Screen", "general");
        //StatusMsg("Loading title");
        // ~~~ breaks in german rom for some reason
        title.LoadTitleScreen(currentProject);
        LoggingServer.logDebug("Loading Tree Graphic", "general");
        title.LoadTreeGraphic(currentRom);
        //StatusMsg("tileset pal");
        LoggingServer.logDebug("Initializing tileset stuff", "general");
        tilesetpal.init();
        //StatusMsg("tile16");
        tile16.init();
        //StatusMsg("tile8");
        tile8.init();
        //StatusMsg("rawtile");
        rawtile.init();
        //StatusMsg("WM draw");
        LoggingServer.logDebug("Initializing world map tiles", "general");
        worldMapTileEditor.DrawTiles();
        //StatusMsg("hex");
        LoggingServer.logDebug("Initializing more stuff", "general");
        hex.LoadFromRom();
        LoggingServer.logDebug("Loading misc settings", "general");
        miscSettingsEditor.loadMiscSettings(currentProject);
        //StatusMsg("patches");
        //patches.LoadAppliedPatches();
        //StatusMsg("font");
        LoggingServer.logDebug("Loading Font", "general");
        menufont.LoadFont(currentProject);
        LoggingServer.logDebug("Loading Menus", "general");
        menufont.LoadMenus(currentProject);
        LoggingServer.logDebug("Loading Character Stats", "general");
        charstats.loadStats(currentProject);
        LoggingServer.logDebug("Loading Enemy Stats", "general");
        enemystats.LoadEnemies();
        LoggingServer.logDebug("Loading Armor", "general");
        SomArmorLoader armorLoader = new SomArmorLoader(armorEditor);
        armorLoader.LoadArmor(currentProject);
        armorEditor.ShowArmor(0);
        LoggingServer.logDebug("Loading Spells", "general");
        SomSpellLoader spellLoader = new SomSpellLoader(spellEditor);
        spellLoader.LoadSpells(currentProject);
        spellEditor.ShowSpell(0);
        LoggingServer.logDebug("Loading Weapons", "general");
        SomWeaponLoader weaponLoader = new SomWeaponLoader(weaponEditor);
        weaponLoader.LoadWeapons(currentProject);
        weaponEditor.ShowWeapon(0);
        LoggingServer.logDebug("Initializing Menu/Font Editor", "general");
        menufont.DrawRingMenuTile();
        menufont.SetPalette();

        //SpriteAnimation.LoadAnims();

        for (int i = 0; i < 8; i++)
        {

            //SNESSample.FSOUND_PlaySound(i, samplec.allSamples[0].FSOUND_sample);
            //SNESSample.FSOUND_SetPaused(i, 1);
            //int fx = SNESSample.FSOUND_FX_Enable(i, 8);
            //SNESSample.FSOUND_FX_SetWavesReverb(fx, -96, -96, 1000, (float).5);
            //SNESSample.FSOUND_SetPaused(i, 0);
        }

        return true;
    }

    public void LoadMopsPatches()
    {
        //string _base = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
        System.Reflection.Assembly a = System.Reflection.Assembly.GetExecutingAssembly();
        //System.Resources.ResourceReader r = new System.Resources.ResourceReader("test");
        //string type;
        //byte[] data;
        //r.GetResourceData("test", out type, out data);
        //Stream s = new MemoryStream(data);
        //string[] n = a.GetManifestResourceNames();
        //Someditc.Properties.Resources.resources
        // As a note: _base = "Someditc.Properties.Resources.resources" or some shit -Enk
        //System.Reflection.ManifestResourceInfo m = a.GetManifestResourceInfo(_base + ".test");
        Stream s = a.GetManifestResourceStream("Someditc.Resources.test.s_p");
            BinaryReader b = new BinaryReader(s);
            patches.LoadPatchFile(b);
            s = a.GetManifestResourceStream("Someditc.Resources.mode7.s_p");
            b = new BinaryReader(s);
            patches.LoadPatchFile(b);
        }
        */
    }
}
